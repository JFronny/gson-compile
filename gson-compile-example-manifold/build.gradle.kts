plugins {
    java
    id("jf.manifold")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    annotationProcessor(project(":gson-compile-processor"))
    compileOnly(project(":gson-compile-annotations"))
    implementation(project(":gson-compile-core"))
}