package io.gitlab.jfronny.gson.compile.example.manifold;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    @GSerializable
    public record ExampleRecord(String yes) {
    }
}