package io.gitlab.jfronny.gson.compile.annotations;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface GSerializable {
    /**
     * @return The class implementing this types serialization/deserialization. Must have static read/write methods
     */
    Class<?> with() default void.class;

    /**
     * @return The builder class to use for creating this type. Incompatible with custom serialization
     */
    Class<?> builder() default void.class;

    /**
     * @return The class to use for configuring JsonReader/JsonWriter instances from convenience methods in the generated class. Must have static configure methods for both.
     */
    Class<?> configure() default void.class;

    /**
     * @return Whether to generate an adapter class to use with normal gson adapter resolution
     */
    boolean generateAdapter() default false;

    /**
     * @return Whether to serialize static fields/methods. Incompatible with generateAdapter and builder
     */
    boolean isStatic() default false;
}
