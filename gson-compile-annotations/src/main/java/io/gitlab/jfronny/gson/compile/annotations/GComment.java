package io.gitlab.jfronny.gson.compile.annotations;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.SOURCE)
public @interface GComment {
    String value();
}
