plugins {
    `java-library`
    id("jf.maven-publish")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation("io.gitlab.jfronny:commons:1.4-SNAPSHOT")
    api("io.gitlab.jfronny:commons-gson:1.4-SNAPSHOT")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}
