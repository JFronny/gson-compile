package io.gitlab.jfronny.gson.compile.core;

import io.gitlab.jfronny.commons.serialize.gson.api.v2.GsonHolders;
import io.gitlab.jfronny.gson.stream.JsonReader;
import io.gitlab.jfronny.gson.stream.JsonWriter;

public class DefaultConfiguration {
    public static class Api {
        public static void configure(JsonWriter writer) {
            GsonHolders.API.getGson().configure(writer);
        }

        public static void configure(JsonReader reader) {
            GsonHolders.API.getGson().configure(reader);
        }
    }

    public static class Config {
        public static void configure(JsonWriter writer) {
            GsonHolders.CONFIG.getGson().configure(writer);
        }

        public static void configure(JsonReader reader) {
            GsonHolders.CONFIG.getGson().configure(reader);
        }
    }
}
