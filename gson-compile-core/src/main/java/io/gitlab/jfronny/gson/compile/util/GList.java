package io.gitlab.jfronny.gson.compile.util;

import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;
import io.gitlab.jfronny.gson.stream.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for generating lists, to be used when data with an array root element
 */
public class GList {
    public static <T> List<T> read(JsonReader reader, ThrowingFunction<JsonReader, T, IOException> read) throws IOException {
        if (reader.isLenient() && reader.peek() != JsonToken.BEGIN_ARRAY) return List.of(read.apply(reader));
        reader.beginArray();
        List<T> res = new LinkedList<>();
        while (reader.hasNext()) {
            if (reader.peek() == JsonToken.NULL) {
                reader.nextNull();
                res.add(null);
            } else res.add(read.apply(reader));
        }
        reader.endArray();
        return res;
    }

    public static <T> void write(JsonWriter writer, List<T> list, ThrowingBiConsumer<T, JsonWriter, IOException> write) throws IOException {
        writer.beginArray();
        for (T t : list) write.accept(t, writer);
        writer.endArray();
    }
}
