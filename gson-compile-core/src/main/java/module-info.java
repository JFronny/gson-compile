module io.gitlab.jfronny.gson.compile.core {
    requires io.gitlab.jfronny.commons.gson;
    requires io.gitlab.jfronny.gson;
    requires io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.gson.compile.core;
    exports io.gitlab.jfronny.gson.compile.util;
}