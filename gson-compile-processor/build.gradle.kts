plugins {
    `java-library`
    id("jf.maven-publish")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation(project(":gson-compile-processor-core"))
    implementation(project(":gson-compile-annotations"))
    implementation("org.jetbrains:annotations:24.0.0")
    implementation("io.gitlab.jfronny:commons:1.4-SNAPSHOT")
    implementation("com.squareup:javapoet:1.13.0")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}
