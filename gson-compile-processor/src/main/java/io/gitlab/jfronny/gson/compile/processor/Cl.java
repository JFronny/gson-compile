package io.gitlab.jfronny.gson.compile.processor;

import com.squareup.javapoet.ClassName;

public class Cl {
    public static final ClassName TYPE_ADAPTER = ClassName.get("io.gitlab.jfronny.gson", "TypeAdapter");
    public static final ClassName TYPE_ADAPTER_FACTORY = ClassName.get("io.gitlab.jfronny.gson", "TypeAdapterFactory");
    public static final ClassName GSON_ELEMENT = ClassName.get("io.gitlab.jfronny.gson", "JsonElement");
    public static final ClassName GSON_WRITER = ClassName.get("io.gitlab.jfronny.gson.stream", "JsonWriter");
    public static final ClassName GSON_READER = ClassName.get("io.gitlab.jfronny.gson.stream", "JsonReader");
    public static final ClassName GSON_TREE_READER = ClassName.get("io.gitlab.jfronny.gson.stream", "JsonTreeReader");
    public static final ClassName GSON_TREE_WRITER = ClassName.get("io.gitlab.jfronny.gson.stream", "JsonTreeWriter");
    public static final ClassName TYPE_TOKEN = ClassName.get("io.gitlab.jfronny.gson.reflect", "TypeToken");
    public static final ClassName GSON_TOKEN = ClassName.get("io.gitlab.jfronny.gson.stream", "JsonToken");
    public static final ClassName SERIALIZED_NAME = ClassName.get("io.gitlab.jfronny.gson.annotations", "SerializedName");
    public static final ClassName GSON_SYNTAX_EXCEPTION = ClassName.get("io.gitlab.jfronny.gson", "JsonSyntaxException");

    public static final ClassName GISO8601UTILS = ClassName.get("io.gitlab.jfronny.gson.util", "ISO8601Utils");
    public static final ClassName CCORE = ClassName.get("io.gitlab.jfronny.gson.compile.core", "CCore");
    public static final ClassName GCOMMENT = ClassName.get("io.gitlab.jfronny.gson.compile.annotations", "GComment");
    public static final ClassName GWITH = ClassName.get("io.gitlab.jfronny.gson.compile.annotations", "GWith");

    public static final ClassName MANIFOLD_EXTENSION = ClassName.get("manifold.ext.rt.api", "Extension");
    public static final ClassName MANIFOLD_THIS = ClassName.get("manifold.ext.rt.api", "This");
}
