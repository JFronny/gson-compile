package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import io.gitlab.jfronny.gson.compile.processor.adapter.Adapter;

public class PrimitiveAdapter extends Adapter<PrimitiveAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return unboxedType.getKind().isPrimitive();
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("writer.value(");
            writeGet.run();
            code.add(");\n");
        }

        @Override
        public void generateRead() {
            code.add(switch (unboxedType.getKind()) {
                case BOOLEAN -> "reader.nextBoolean()";
                case BYTE -> "(byte) reader.nextInt()";
                case SHORT -> "(short) reader.nextInt()";
                case INT -> "reader.nextInt()";
                case LONG -> "reader.nextLong()";
                case CHAR -> "(char) reader.nextInt()";
                case FLOAT -> "(float) reader.nextDouble()";
                case DOUBLE -> "reader.nextDouble()";
                default -> throw new IllegalArgumentException("Unsupported primitive: " + unboxedType.getKind());
            });
        }
    }
}
