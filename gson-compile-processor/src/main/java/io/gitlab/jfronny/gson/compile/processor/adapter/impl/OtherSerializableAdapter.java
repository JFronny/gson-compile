package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import io.gitlab.jfronny.gson.compile.processor.SerializableClass;
import io.gitlab.jfronny.gson.compile.processor.adapter.AdapterAdapter;

public class OtherSerializableAdapter extends AdapterAdapter<OtherSerializableAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends AdapterAdapter<Hydrated>.Hydrated {
        private ClassName adapter;

        @Override
        public boolean applies() {
            return adapter != null;
        }

        @Override
        protected ClassName createAdapter(String name) {
            return adapter;
        }

        @Override
        protected void afterHydrate() {
            for (SerializableClass adapter : other) {
                if (TypeName.get(adapter.classElement().asType()).equals(typeName)) {
                    // Use self-made adapter
                    this.adapter = adapter.generatedClassName();
                }
            }
        }
    }
}
