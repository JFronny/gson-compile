package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import com.squareup.javapoet.*;
import io.gitlab.jfronny.gson.compile.processor.Cl;
import io.gitlab.jfronny.gson.compile.processor.adapter.AdapterAdapter;

import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;

public class ReflectAdapter extends AdapterAdapter<ReflectAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends AdapterAdapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return !options.containsKey("gsonCompileNoReflect");
        }

        @Override
        protected String createAdapter(String typeAdapterName) {
            message.printMessage(Diagnostic.Kind.WARNING, "Falling back to adapter detection for unsupported type " + type, sourceElement);
            TypeName typeAdapterType = ParameterizedTypeName.get(Cl.TYPE_ADAPTER, typeName);
            CodeBlock.Builder block = CodeBlock.builder();
            block.add("$T.HOLDER.getGson().getAdapter(", Cl.CCORE);
            appendFieldTypeToken(true);
            block.add(")");
            klazz.addField(
                    FieldSpec.builder(typeAdapterType, typeAdapterName)
                            .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                            .initializer(block.build())
                            .build()
            );
            return typeAdapterName;
        }
    }
}
