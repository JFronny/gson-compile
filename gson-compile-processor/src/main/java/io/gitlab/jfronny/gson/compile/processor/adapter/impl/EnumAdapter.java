package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import com.squareup.javapoet.*;
import io.gitlab.jfronny.gson.compile.processor.core.TypeHelper;
import io.gitlab.jfronny.gson.compile.processor.adapter.Adapter;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.type.DeclaredType;

public class EnumAdapter extends Adapter<EnumAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<Hydrated>.Hydrated {
        private DeclaredType tel;

        @Override
        protected void afterHydrate() {
            super.afterHydrate();
            tel = null;
            DeclaredType declared = TypeHelper.asDeclaredType(type);
            if (declared == null) return;
            if (declared.asElement().getKind() != ElementKind.ENUM) return;
            tel = declared;
        }

        @Override
        public boolean applies() {
            return tel != null;
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("writer.value(");
            writeGet.run();
            code.add(".name());\n");
        }

        @Override
        public void generateRead() {
            String methodName = "read$" + name;
            klazz.addMethod(
                    MethodSpec.methodBuilder(methodName)
                            .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                            .returns(TypeName.get(tel))
                            .addParameter(String.class, "value")
                            .addCode(
                                    CodeBlock.builder()
                                            .beginControlFlow("for ($1T t : $1T.values())", tel)
                                            .addStatement("if (t.name().equals(value)) return t")
                                            .endControlFlow()
                                            .addStatement("return null")
                                            .build()
                            )
                            .build()
            );
            code.add("$N(reader.nextString())", methodName);
        }
    }
}
