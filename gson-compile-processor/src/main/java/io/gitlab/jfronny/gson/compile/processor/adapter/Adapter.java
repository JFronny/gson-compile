package io.gitlab.jfronny.gson.compile.processor.adapter;

import com.squareup.javapoet.*;
import io.gitlab.jfronny.gson.compile.processor.SerializableClass;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.util.*;

public abstract class Adapter<T extends Adapter<T>.Hydrated> {
    protected Messager message;
    protected Types typeUtils;
    protected Map<String, String> options;

    public abstract T instantiate();

    public void init(ProcessingEnvironment env) {
        this.message = env.getMessager();
        this.typeUtils = env.getTypeUtils();
        this.options = env.getOptions();
    }

    public final T hydrate(TypeSpec.Builder klazz, CodeBlock.Builder code, List<TypeVariableName> typeVariables, Set<SerializableClass> other, TypeMirror type, String propName, List<? extends AnnotationMirror> annotations, Element sourceElement) {
        T instance = instantiate();
        instance.klazz = klazz;
        instance.typeVariables = typeVariables;
        instance.other = other;
        instance.type = type;
        instance.code = code;
        instance.unboxedType = instance.unbox(type);
        instance.name = propName;
        instance.argName = "_" + propName;
        instance.adapterName = "adapter_" + propName;
        instance.typeName = TypeName.get(type).box();
        instance.annotations = annotations;
        instance.sourceElement = sourceElement;
        instance.afterHydrate();
        return instance;
    }

    public abstract class Hydrated {
        protected TypeSpec.Builder klazz;
        protected List<TypeVariableName> typeVariables;
        protected Set<SerializableClass> other;
        protected TypeMirror type;
        protected TypeMirror unboxedType;
        protected CodeBlock.Builder code;
        protected String name;
        protected String argName;
        protected String adapterName;
        protected TypeName typeName;
        protected List<? extends AnnotationMirror> annotations;
        protected Element sourceElement;

        public abstract boolean applies();
        public abstract void generateWrite(Runnable writeGet);
        public abstract void generateRead();
        protected void afterHydrate() {}

        protected void generateRead(CodeBlock.Builder code, TypeMirror type, String name, List<? extends AnnotationMirror> annotations) {
            Adapters.generateRead(klazz, code, typeVariables, other, type, name, annotations, sourceElement, message);
        }

        protected void generateWrite(CodeBlock.Builder code, TypeMirror type, String name, List<? extends AnnotationMirror> annotations, Runnable writeGet) {
            Adapters.generateWrite(klazz, code, typeVariables, other, type, name, annotations, sourceElement, message, writeGet);
        }

        protected TypeMirror unbox(TypeMirror type) {
            try {
                return typeUtils.unboxedType(type);
            } catch (IllegalArgumentException e) {
                return type;
            }
        }
    }
}
