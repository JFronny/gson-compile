package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import io.gitlab.jfronny.gson.compile.processor.Cl;
import io.gitlab.jfronny.gson.compile.processor.adapter.Adapter;

import javax.lang.model.element.Modifier;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;

public class DateAdapter extends Adapter<DateAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return type.toString().equals(Date.class.getCanonicalName());
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("writer.value($T.format(", Cl.GISO8601UTILS);
            writeGet.run();
            code.add("));\n");
        }

        @Override
        public void generateRead() {
            boolean found = false;
            for (MethodSpec spec : klazz.methodSpecs) {
                if (spec.name.equals("parseDate")) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                klazz.addMethod(
                        MethodSpec.methodBuilder("parseDate")
                                .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                                .returns(Date.class)
                                .addParameter(String.class, "date")
                                .addCode(
                                        CodeBlock.builder()
                                                .beginControlFlow("try")
                                                .addStatement("return $T.parse(date, new $T(0))", Cl.GISO8601UTILS, ParsePosition.class)
                                                .nextControlFlow("catch ($T e)", ParseException.class)
                                                .addStatement("throw new $T(\"Failed Parsing '\" + date + \"' as Date\", e)", Cl.GSON_SYNTAX_EXCEPTION)
                                                .endControlFlow()
                                                .build()
                                )
                                .build()
                );
            }

            code.add("parseDate(reader.nextString())");
        }
    }
}
