package io.gitlab.jfronny.gson.compile.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import io.gitlab.jfronny.gson.compile.processor.core.value.ElementException;
import org.jetbrains.annotations.Nullable;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public record SerializableClass(TypeElement classElement, ClassName generatedClassName, @Nullable TypeMirror adapter, @Nullable TypeMirror builder, @Nullable TypeMirror configure, boolean generateAdapter, boolean isStatic) {
    public static SerializableClass of(TypeElement element, @Nullable TypeMirror with, @Nullable TypeMirror builder, @Nullable TypeMirror configure, boolean generateAdapter, boolean isStatic, boolean manifold) throws ElementException {
        ClassName className = ClassName.get(element);
        String pkg = manifold ? "gsoncompile.extensions." + className.packageName() + '.' + className.simpleNames().get(0) : className.packageName();
        ClassName generatedClassName = ClassName.get(pkg, "GC_" + className.simpleNames().get(0), className.simpleNames().subList(1, className.simpleNames().size()).toArray(String[]::new));
        return new SerializableClass(element, generatedClassName, voidToNull(with), voidToNull(builder), voidToNull(configure), generateAdapter, isStatic).validate();
    }

    public ClassName getClassName() {
        return ClassName.get(classElement);
    }

    public TypeName getTypeName() {
        return TypeName.get(classElement.asType());
    }

    private SerializableClass validate() throws ElementException {
        if (adapter != null && builder != null) throw new ElementException("@GSerializable with both an adapter and a builder. This is unsupported!", classElement);
        if (isStatic && builder != null) throw new ElementException("@GSerializable which is static and has a builder. This is unsupported!", classElement);
        if (isStatic && generateAdapter) throw new ElementException("@GSerializable which is static and generates an adapter. This is unsupported!", classElement);
        return this;
    }

    private static TypeMirror voidToNull(TypeMirror type) {
        return type == null || type.toString().equals("void") ? null : type;
    }
}
