package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import io.gitlab.jfronny.gson.compile.processor.adapter.Adapter;

public class StringAdapter extends Adapter<StringAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<Hydrated>.Hydrated {
        @Override
        public boolean applies() {
            return type.toString().equals(String.class.getCanonicalName());
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("writer.value(");
            writeGet.run();
            code.add(");\n");
        }

        @Override
        public void generateRead() {
            code.add("reader.nextString()");
        }
    }
}
