package io.gitlab.jfronny.gson.compile.processor.adapter.impl;

import com.squareup.javapoet.*;
import io.gitlab.jfronny.gson.compile.processor.Cl;
import io.gitlab.jfronny.gson.compile.processor.core.TypeHelper;
import io.gitlab.jfronny.gson.compile.processor.adapter.Adapter;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.TypeMirror;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ArrayAdapter extends Adapter<ArrayAdapter.Hydrated> {
    @Override
    public Hydrated instantiate() {
        return new Hydrated();
    }

    public class Hydrated extends Adapter<ArrayAdapter.Hydrated>.Hydrated {
        private ArrayType type;
        private TypeMirror componentType;

        @Override
        public boolean applies() {
            return type != null;
        }

        @Override
        protected void afterHydrate() {
            type = TypeHelper.asArrayType(super.type);
            componentType = type == null ? null : type.getComponentType();
        }

        @Override
        public void generateWrite(Runnable writeGet) {
            code.add("for ($T $N : ", componentType, argName);
            writeGet.run();
            code.beginControlFlow(")")
                    .beginControlFlow("if ($N == null)", argName)
                    .addStatement("if (writer.getSerializeNulls()) writer.nullValue()")
                    .nextControlFlow("else");
            generateWrite(code, componentType, argName, componentType.getAnnotationMirrors(), () -> code.add(argName));
            code.endControlFlow().endControlFlow();
        }

        @Override
        public void generateRead() {
            CodeBlock.Builder kode = CodeBlock.builder();
            // Coerce
            kode.beginControlFlow("if (reader.isLenient() && reader.peek() != $T.BEGIN_ARRAY)", Cl.GSON_TOKEN)
                    .add("return new $T[] { ", componentType);
            generateRead(kode, componentType, argName, componentType.getAnnotationMirrors());
            kode.add(" };\n").endControlFlow();

            kode.addStatement("$T<$T> list = new $T<>()", List.class, componentType, ArrayList.class)
                    .addStatement("reader.beginArray()")
                    .beginControlFlow("while (reader.hasNext())")
                    .beginControlFlow("if (reader.peek() == $T.NULL)", Cl.GSON_TOKEN)
                    .addStatement("reader.nextNull()")
                    .addStatement("list.add(null)")
                    .nextControlFlow("else")
                    .add("list.add(");
            generateRead(kode, componentType, argName, componentType.getAnnotationMirrors());
            kode.add(");\n")
                    .endControlFlow()
                    .endControlFlow()
                    .addStatement("reader.endArray()")
                    .addStatement("return list.toArray($T[]::new)", componentType);

            String methodName = "read$" + name;
            klazz.addMethod(
                    MethodSpec.methodBuilder(methodName)
                            .addModifiers(Modifier.PRIVATE, Modifier.STATIC)
                            .returns(typeName)
                            .addParameter(Cl.GSON_READER, "reader")
                            .addException(IOException.class)
                            .addCode(kode.build())
                            .build()
            );
            code.add("$N(reader)", methodName);
        }
    }
}
