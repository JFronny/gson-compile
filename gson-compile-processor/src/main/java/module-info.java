module io.gitlab.jfronny.gson.compile.processor {
    requires com.squareup.javapoet;
    requires java.compiler;
    requires io.gitlab.jfronny.gson.compile.processor.core;
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.gson.compile.annotations;
    requires static org.jetbrains.annotations;
}