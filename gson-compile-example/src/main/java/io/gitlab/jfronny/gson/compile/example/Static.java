package io.gitlab.jfronny.gson.compile.example;

import io.gitlab.jfronny.gson.compile.annotations.GSerializable;

@GSerializable(isStatic = true, configure = Main.Configuration.class)
public class Static {
    public boolean nonStatic;
    public static boolean joe;
}
