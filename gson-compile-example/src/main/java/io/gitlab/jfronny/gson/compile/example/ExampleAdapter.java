package io.gitlab.jfronny.gson.compile.example;

import io.gitlab.jfronny.gson.stream.JsonReader;
import io.gitlab.jfronny.gson.stream.JsonWriter;

import java.io.IOException;

public class ExampleAdapter {
    public static void write(boolean bool, JsonWriter writer) throws IOException {
        writer.value(!bool);
    }

    public static boolean read(JsonReader reader) throws IOException {
        return !reader.nextBoolean();
    }
}
