package io.gitlab.jfronny.gson.compile.example;

import io.gitlab.jfronny.gson.annotations.SerializedName;
import io.gitlab.jfronny.gson.compile.annotations.*;
import io.gitlab.jfronny.gson.stream.JsonReader;
import io.gitlab.jfronny.gson.stream.JsonWriter;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    @GSerializable(generateAdapter = true)
    public static class ExamplePojo {
        @SerializedName("someBalue")
        public String someValue;
        @GComment("Yes!")
        public Boolean someBool;

        @GComment("Halal")
        @SerializedName("bingChiller")
        public String getBass() {
            return "Yes";
        }

        public ExamplePojo2 nested;

        public Set<ExamplePojo> recursive1;

        public LinkedList<ExampleRecord> recursive2;

        public Queue<String> queue;

        public Date date;

        public EeE eEe;

        public UnsupportedClass unsupported;

        public String getUnsupported() {
            return unsupported == null ? null : unsupported.text;
        }

        public void setUnsupported(String text) {
            unsupported = new UnsupportedClass(text);
        }

        public void setJoe(String joe) {
        }

        public String getJoe() {
            return "A";
        }
    }

    @GSerializable(configure = Configuration.class)
    public static class ExamplePojo2 {
        @GComment("Yes!")
        @GWith(serializer = ExampleAdapter.class)
        public boolean primitive;
        public ExamplePojo2[] recursiveTest;

        public Map<EeE, Example4> map1;

        public Map<String, String> map2;

        public Map<Integer, String> map3;

        public Map<UUID, String> map4;

        public Inner inner;

        @GSerializable
        public record Inner(String s, Inner2 inner2) {
            @GSerializable
            public record Inner2(String s) {}
        }
    }

    @GSerializable
    public record ExampleRecord(String hello, @GComment("Sheesh") ExamplePojo2 pojo) {
        @GPrefer
        public ExampleRecord(String yes) {
            this(yes, null);
        }
    }

    @GSerializable(builder = Example4.Builder.class)
    public static class Example4 {
        public String someField;
        public boolean shesh;

        public static class Builder {
            private String someField;
            private boolean shesh;

            public Builder(String someField) {
                this.someField = someField;
            }

            public Builder setShesh(boolean shesh) {
                this.shesh = shesh;
                return this;
            }

            public Example4 build() {
                Example4 e = new Example4();
                e.someField = someField;
                return e;
            }
        }
    }

    public enum EeE {
        Yes, Yay, Aaee;
    }

    public static class UnsupportedClass {
        private final String text;

        public UnsupportedClass(String text) {
            this.text = text;
        }
    }

    public static class Configuration {
        public static void configure(JsonWriter writer) {

        }

        public static void configure(JsonReader reader) {

        }
    }
}