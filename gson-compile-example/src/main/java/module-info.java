module io.gitlab.jfronny.gson.compile.example {
    requires static io.gitlab.jfronny.gson.compile.annotations;
    requires io.gitlab.jfronny.gson;
    exports io.gitlab.jfronny.gson.compile.example;
}