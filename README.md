# Gson-Compile
A java annotation processor implementing automated type adapter generation for gson-comments.
The goal of this AP is to
- Support common features such as getters, setters, fields, constructors
- Support modern language features such as records
- Support json5 through gson-comments
- Be compile-time where possible (ideally compatible with proguard)

## Supported types
- Primitives (and boxes)
- Records
- Nested serializable types
- Arrays
- Collections (Sets, Lists, Queues, Deques)
- java.util.Date as iso8601
- Enums
- Maps with string, primitive, enum or UUID keys

## Used properties
Use `@GPrefer` to choose one construction method if multiple are available
- Builders (referenced in `@GSerializable`)
- Factory functions
- Constructors
- Getters/Setters
- Direct fields (public non-static only)

## Additional features
- Support for generating Gson adapters to hook into the gson system
- Optional, strict no-reflection enforcement via `-AgsonCompileNoReflect`
- Comments via `@GComment`
- Several utility methods in the generated class for reading from/writing to various sources
- Support for generating extension methods if manifold is present

## TODO
- Support for nested types from libraries
- Static classes (for configs)

## Credit
- The Gson-Compile processor is based on [gsonvalue](https://github.com/evant/gsonvalue) and [value-processor](https://github.com/evant/value-processor) by Eva Tatarka.
- The API was inspired by [kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization) by Jetbrains
- The serialization is powered by [my fork](https://gitlab.com/JFronny/gson-comments) of [gson](https://github.com/google/gson) by Google
