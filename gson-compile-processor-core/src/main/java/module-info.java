module io.gitlab.jfronny.gson.compile.processor.core {
    requires java.compiler;
    requires static org.jetbrains.annotations;
    requires io.gitlab.jfronny.commons;
    exports io.gitlab.jfronny.gson.compile.processor.core;
    exports io.gitlab.jfronny.gson.compile.processor.core.value;
}