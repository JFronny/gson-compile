package io.gitlab.jfronny.gson.compile.processor.core;

import java.util.Comparator;
import java.util.List;

public enum StringListComparator implements Comparator<List<String>> {
    INSTANCE;

    @Override
    public int compare(List<String> left, List<String> right) {
        int dif = left.size() - right.size();
        if (dif != 0) return dif;
        for (int i = 0; i < left.size(); i++) {
            dif = left.get(i).compareTo(right.get(i));
            if (dif != 0) return dif;
        }
        return 0;
    }
}
