package io.gitlab.jfronny.gson.compile.processor.core;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SupportedAnnotationTypes2 {
    Class<?>[] value();
}
