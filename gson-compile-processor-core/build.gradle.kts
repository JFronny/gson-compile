plugins {
    `java-library`
    id("jf.maven-publish")
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation("org.jetbrains:annotations:24.0.0")
    implementation("io.gitlab.jfronny:commons:1.4-SNAPSHOT")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}
