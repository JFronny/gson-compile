group = "io.gitlab.jfronny.gson"
version = "1.4-SNAPSHOT"

subprojects {
    group = rootProject.group
    version = rootProject.version
}