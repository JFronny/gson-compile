pluginManagement {
    repositories {
        maven("https://maven.frohnmeyer-wds.de/artifacts")
        mavenCentral()
    }
    plugins {
        id("jf.maven-publish") version "1.5-SNAPSHOT"
        id("jf.manifold") version "1.5-SNAPSHOT"
    }
}

rootProject.name = "gson-compile"
include("gson-compile-core")
include("gson-compile-processor")
include("gson-compile-annotations")
include("gson-compile-example")
include("gson-compile-example-manifold")
include("gson-compile-processor-core")
